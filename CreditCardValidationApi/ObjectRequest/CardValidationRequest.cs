﻿
using System.ComponentModel.DataAnnotations;

namespace CreditCardValidationApi.ObjectRequest
{
    public class CardValidationRequest
    {
        [Required]
        public long CardNumber { get; set; }

        [Required, StringLength(6, ErrorMessage = "The expired date must be at least 6 characters long.", MinimumLength = 6)]
        public string ExpiredDate { get; set; }
    }

}