﻿using CreditCardService.ObjectResponse;
using CreditCardValidationApi.Interface;
using CreditCardValidationApi.ObjectRequest;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CreditCardValidationApi.Controllers
{

    /// <summary>
    /// Card controller api
    /// </summary>
    public class CardController : ApiController
    {
        ICardProcess _cardProcessService;

        /// <summary>
        /// Card controller constructor
        /// </summary>
        /// <param name="cardProcessService"></param>
        public CardController(ICardProcess cardProcessService)
        {
            this._cardProcessService = cardProcessService;
        }

        /// <summary>
        /// This function to validate credit card.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Validation(CardValidationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _cardProcessService.ProcessAsync(request);
            return Ok(result);
        }
    }
}
