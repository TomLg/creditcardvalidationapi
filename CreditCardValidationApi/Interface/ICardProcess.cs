﻿using CreditCardService.ObjectResponse;
using CreditCardValidationApi.ObjectRequest;
using System.Threading.Tasks;

namespace CreditCardValidationApi.Interface
{
    public interface ICardProcess
    {
        Task<CardValidationResponse> ProcessAsync(CardValidationRequest request);

    }
}
