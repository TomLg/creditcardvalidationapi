﻿using CreditCardService.Enum;
using CreditCardService.Interface;
using CreditCardService.ObjectResponse;
using CreditCardValidationApi.Interface;
using CreditCardValidationApi.ObjectRequest;
using System;
using System.Threading.Tasks;

namespace CreditCardValidationApi.Process
{
    public class CardProcess : ICardProcess
    {
        ICreditCardTypeRule _creditCardRuleService;

        public CardProcess(ICreditCardTypeRule creditCardRuleService)
        {
            this._creditCardRuleService = creditCardRuleService;
        }

        public async Task<CardValidationResponse> ProcessAsync(CardValidationRequest request)
        {
            CardValidationResponse response = null;
            if (IsValidNumber(request.ExpiredDate))
            {
                response = await _creditCardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);
            }
            else
            {
                response = new CardValidationResponse
                {
                    CardType = CardType.Unknown.ToString(),
                    Result = "Invalid",
                    ErrorMessage = "Invalid expired date"
                };
            }

            return response;
        }


        /// <summary>
        /// Check if input number is only digit.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private bool IsValidNumber(string data)
        {
            foreach (char c in data)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

    }
}