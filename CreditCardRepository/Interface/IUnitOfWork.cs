﻿
using System;
namespace CreditCardRepository.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        ICreditCardTypeRuleRepository CreditCardTypeRule { get; }
        ICardRepository CardRepository { get; }
        int Complete();

    }
}
