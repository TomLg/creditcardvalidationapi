﻿
using CreditCardDataEF;

namespace CreditCardRepository.Interface
{
   public interface ICreditCardTypeRuleRepository : IRepository<CreditCardTypeRule>
    {
    }
}
