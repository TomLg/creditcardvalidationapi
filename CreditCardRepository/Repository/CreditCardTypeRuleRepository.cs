﻿using CreditCardDataEF;
using CreditCardRepository.Interface;
namespace CreditCardRepository.Repository
{
   public class CreditCardTypeRuleRepository : Repository<CreditCardTypeRule>, ICreditCardTypeRuleRepository
    {
        public CreditCardTypeRuleRepository(Entities context)
            : base(context)
        {}

        public Entities Entities
        {
            get { return Context as Entities; }
        }
    }
}
