﻿using CreditCardDataEF;
using CreditCardRepository.Interface;
namespace CreditCardRepository.Repository
{
    public class CardRepository : Repository<usp_GetCreditCard_Result>, ICardRepository
    {

        public CardRepository(Entities context)
    : base(context)
        { }

        public Entities Entities
        {
            get { return Context as Entities; }
        }

    }
}
