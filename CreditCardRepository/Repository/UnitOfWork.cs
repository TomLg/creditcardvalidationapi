﻿using CreditCardDataEF;
using CreditCardRepository.Interface;
using System;
using System.Data.Entity.Infrastructure;

namespace CreditCardRepository.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Entities _context;

        public UnitOfWork(Entities context)
        {
            _context = context;
            CreditCardTypeRule = new CreditCardTypeRuleRepository(_context);
            CardRepository = new CardRepository(_context);

        }
        public ICreditCardTypeRuleRepository CreditCardTypeRule { get; private set; }

        public ICardRepository CardRepository { get; private set; }

        public int Complete()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException) { return -1; }
            catch (Exception) { return 0; }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
