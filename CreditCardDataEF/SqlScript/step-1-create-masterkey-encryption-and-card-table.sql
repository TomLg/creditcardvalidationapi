
--CREATE MASTER KEY ENCRYPTION BY
--PASSWORD = 'Password$1'
--GO


--CREATE CERTIFICATE SelfSignedCertificate  
--WITH SUBJECT = 'Password Encryption';  
--GO  
--CREATE SYMMETRIC KEY SQLSymmetricKey  
--WITH ALGORITHM = AES_128  
--ENCRYPTION BY CERTIFICATE SelfSignedCertificate;  
--GO  


--OPEN SYMMETRIC KEY SQLSymmetricKey  
--DECRYPTION BY CERTIFICATE SelfSignedCertificate; 


--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('341449635398431','042020',1)
--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('3530111333300000','052028',1)
--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('3566002020360505','062023',1)
--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('371449635398431','032021',1)
--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('4012888888881881','052020',1)
--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('4111111111111111','032020',1)
--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('5105105105105100','092024',1)
--INSERT INTO Card(CardNumber,ExpiredDate,IsActive) VALUES('5555555555554444','062039',1)


--UPDATE Card  
--SET [EncryptedCreditCardNumber] = EncryptByKey(Key_GUID('SQLSymmetricKey'), CardNumber);  
--GO  
--select * from Card


--ALTER Table
--   Card
--drop column
--   CardNumber;  


--SELECT ExpiredDate,EncryptedCreditCardNumber,  
--CONVERT(varchar, DecryptByKey(EncryptedCreditCardNumber)) AS 'CardNumber'  
--FROM Card; 
