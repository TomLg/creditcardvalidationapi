GO

/****** Object:  Table [dbo].[CreditCardTypeRule]    Script Date: 12/2/2018 3:22:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CreditCardTypeRule](
	[RuleId] [int] IDENTITY(1,1) NOT NULL,
	[CardType] [nvarchar](15) NULL,
	[DigitStart] [int] NULL,
	[DigitEnd] [int] NULL,
	[IsRuningNumber] [bit] NULL,
	[RegEx] [nvarchar](150) NULL,
	[Length] [int] NULL,
	[ExpireLeapYear] [bit] NULL,
	[ExpirePrimNumber] [bit] NULL,
 CONSTRAINT [PK_CreditCardTypeRule] PRIMARY KEY CLUSTERED 
(
	[RuleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


INSERT INTO CreditCardTypeRule(CardType,DigitStart,DigitEnd,IsRuningNumber,RegEx,Length,ExpireLeapYear,ExpirePrimNumber) 
values('Visa',4,4,1,'',16,1,0),
('Master',5,5,1,'',16,0,1),
('Amex',34,37,0,'^(34|37)',15,0,0),
('JCB',3528,3589,1,'',16,0,0)

