-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usp_GetCreditCard
@CardNumber varchar(16),
@ExpiredDate char(6)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TempCard Table (ExpiredDate char(6),CardNumber varchar(16))

	OPEN SYMMETRIC KEY SQLSymmetricKey  
	DECRYPTION BY CERTIFICATE SelfSignedCertificate; 

    INSERT INTO @TempCard(ExpiredDate, CardNumber)
	SELECT ExpiredDate, CONVERT(varchar, DecryptByKey(EncryptedCreditCardNumber)) AS 'CardNumber' FROM Card WHERE IsActive = 1

	SELECT ExpiredDate, CardNumber FROM @TempCard Where CardNumber = @CardNumber AND ExpiredDate = @ExpiredDate

END
GO

--usp_GetCreditCard '4012888888881881','052020'



