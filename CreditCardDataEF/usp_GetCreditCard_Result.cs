//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CreditCardDataEF
{
    using System;
    
    public partial class usp_GetCreditCard_Result
    {
        public string ExpiredDate { get; set; }
        //public byte[] EncryptedCreditCardNumber { get; set; }
        public string CardNumber { get; set; }
    }
}
