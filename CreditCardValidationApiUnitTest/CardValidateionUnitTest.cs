﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreditCardValidationApi.ObjectRequest;
using System.Threading.Tasks;
using Moq;
using CreditCardService.Interface;
using CreditCardRepository.Interface;
using CreditCardService.Implement;
using System.Collections.Generic;
using CreditCardDataEF;
using System.Linq;
using System.Data.SqlClient;
using System.Data;

namespace CreditCardValidationApiUnitTest
{
    [TestClass]
    public class CardValidateionUnitTest
    {

        private List<CreditCardTypeRule> GetCreditCardRule()
        {
            List<CreditCardTypeRule> cardRuleTypeList = new List<CreditCardTypeRule>();
            cardRuleTypeList.Add(new CreditCardTypeRule
            {
                CardType = "Visa",
                Length = 16,
                DigitStart = 4,
                DigitEnd = 4,
                IsRuningNumber = true,
                RegEx = "",
                ExpireLeapYear = true,
                ExpirePrimNumber = false,
            });
            cardRuleTypeList.Add(new CreditCardTypeRule
            {
                CardType = "Master",
                Length = 16,
                DigitStart = 5,
                DigitEnd = 5,
                IsRuningNumber = true,
                RegEx = "",
                ExpireLeapYear = false,
                ExpirePrimNumber = true,
            });
            cardRuleTypeList.Add(new CreditCardTypeRule
            {
                CardType = "Amex",
                Length = 15,
                DigitStart = 34,
                DigitEnd = 37,
                IsRuningNumber = false,
                RegEx = "^(34|37)",
                ExpireLeapYear = false,
                ExpirePrimNumber = false,
            });
            cardRuleTypeList.Add(new CreditCardTypeRule
            {
                CardType = "JCB",
                Length = 16,
                DigitStart = 3528,
                DigitEnd = 3589,
                IsRuningNumber = true,
                RegEx = "",
                ExpireLeapYear = false,
                ExpirePrimNumber = false,
            });

            return cardRuleTypeList;
        }

        #region valid card
        [TestMethod]
        public async Task Validate_VisaCard_Should_Valid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 4111111111111111, ExpiredDate = "032020" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate};

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("Valid", result.Result);
            Assert.AreEqual("Visa", result.CardType);
        }

        [TestMethod]
        public async Task Validate_MasterCard_Should_Valid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 5105105105105100, ExpiredDate = "092027" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("Valid", result.Result);
            Assert.AreEqual("Master", result.CardType);

        }

        [TestMethod]
        public async Task Validate_AmexCard_Should_Valid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 371449635398431, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("Valid", result.Result);
            Assert.AreEqual("Amex", result.CardType);

        }


        [TestMethod]
        public async Task Validate_JCBCard_Should_Valid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 3530111333300000, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("Valid", result.Result);
            Assert.AreEqual("JCB", result.CardType);

        }

        #endregion

        #region Invalid card

        [TestMethod]
        public async Task Validate_VisaCard_Should_InValid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 4111111111111111, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("InValid", result.Result);
            Assert.AreEqual("Visa", result.CardType);
        }

        [TestMethod]
        public async Task Validate_MasterCard_Should_InValid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 5105105105105100, ExpiredDate = "092020" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("InValid", result.Result);
            Assert.AreEqual("Master", result.CardType);

        }

        [TestMethod]
        public async Task Validate_AmexCard_Should_InValid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            /* Set card number length = 16 should be invalid becase Amex valid lenght is 15*/
            var request = new CardValidationRequest { CardNumber = 3714496353984310, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("InValid", result.Result);
            Assert.AreEqual("Amex", result.CardType);

        }


        [TestMethod]
        public async Task Validate_JCBCard_Should_InValid()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            /* Set card number length = 15 should be invalid becase JCB valid lenght is 16*/

            var request = new CardValidationRequest { CardNumber = 353011133330000, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("InValid", result.Result);
            Assert.AreEqual("JCB", result.CardType);

        }


        [TestMethod]
        public async Task Validate_Card_Return_Unknown()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            /* Set card number length = 15 should be invalid becase JCB valid lenght is 16*/

            var request = new CardValidationRequest { CardNumber = 953011133330000, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = request.CardNumber.ToString(),
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("", result.Result);
            Assert.AreEqual("Unknown", result.CardType);

        }

        #endregion

        #region DoesNotExist

        [TestMethod]
        public async Task Validate_VisaCard_DoesNotExist()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 4111111111111112, ExpiredDate = "032020" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = "4111111111111111",
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("DoesNotExist", result.Result);
            Assert.AreEqual("Visa", result.CardType);
        }

        [TestMethod]
        public async Task Validate_MasterCard_DoesNotExist()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 5105105105105101, ExpiredDate = "092027" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = "5105105105105100",
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("DoesNotExist", result.Result);
            Assert.AreEqual("Master", result.CardType);

        }

        [TestMethod]
        public async Task Validate_AmexCard_DoesNotExist()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 371449635398432, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = "371449635398431",
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("DoesNotExist", result.Result);
            Assert.AreEqual("Amex", result.CardType);

        }


        [TestMethod]
        public async Task Validate_JCBCard_DoesNotExist()
        {

            Mock<ICreditCardTypeRule> cardTypeRule = new Mock<ICreditCardTypeRule>();
            var request = new CardValidationRequest { CardNumber = 3530111333300001, ExpiredDate = "032021" };

            var cardRuleTypeList = GetCreditCardRule();
            List<usp_GetCreditCard_Result> dbResultCardList = new List<usp_GetCreditCard_Result>();
            dbResultCardList.Add(new usp_GetCreditCard_Result
            {
                CardNumber = "3530111333300000",
                ExpiredDate = request.ExpiredDate
            });

            Mock<IUnitOfWork> unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(a => a.CreditCardTypeRule.GetAll()).Returns(cardRuleTypeList.AsEnumerable());

            var param1 = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = request.CardNumber.ToString() };
            var param2 = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = request.ExpiredDate };

            unitOfWork.Setup(a => a.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                , new[] { It.IsAny<SqlParameter>(), It.IsAny<SqlParameter>() })).ReturnsAsync(dbResultCardList.AsQueryable);

            CreditCardTypeRuleService cardRuleService = new CreditCardTypeRuleService(unitOfWork.Object);
            var result = await cardRuleService.CardValidationProcessAsync(request.CardNumber.ToString(), request.ExpiredDate);

            Assert.AreEqual("DoesNotExist", result.Result);
            Assert.AreEqual("JCB", result.CardType);

        }

        #endregion
    }
}
