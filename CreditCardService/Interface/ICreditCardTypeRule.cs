﻿using CreditCardService.ObjectResponse;
using System.Threading.Tasks;

namespace CreditCardService.Interface
{
   public interface ICreditCardTypeRule
    {

        Task<CardValidationResponse> CardValidationProcessAsync(string cardNumber, string expiredDate);

    }
}
