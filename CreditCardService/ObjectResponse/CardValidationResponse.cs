﻿
namespace CreditCardService.ObjectResponse
{
    public class CardValidationResponse
    {
        public string CardType { get; set; }
        public string Result { get; set; }
        public string ErrorMessage { get; set; }
    }
}
