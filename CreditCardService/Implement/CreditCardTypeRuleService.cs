﻿using CreditCardDataEF;
using CreditCardRepository.Interface;
using CreditCardService.Data;
using CreditCardService.Enum;
using CreditCardService.Interface;
using CreditCardService.ObjectResponse;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CreditCardService.Implement
{
    public class CreditCardTypeRuleService : ICreditCardTypeRule
    {
        private IUnitOfWork _unitOfWork;

        public CreditCardTypeRuleService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }


        /// <summary>
        /// Process the credit card validation card number and expired date.
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <param name="expiredDate"></param>
        /// <returns></returns>
        public async Task<CardValidationResponse> CardValidationProcessAsync(string cardNumber, string expiredDate)
        {
            CardValidationResponse validationResult = null;
            var cardValidateRule = _unitOfWork.CreditCardTypeRule.GetAll().ToList();

            var validationRuleFormat = GetCreditCardRuleFormat(cardValidateRule);
            var cardType = GetCardType(validationRuleFormat, cardNumber);

            if (cardType != CardType.Unknown)
            {

                var cardInfo = cardValidateRule.FirstOrDefault(s => String.Equals(s.CardType, cardType.ToString(),
                    StringComparison.CurrentCultureIgnoreCase));

                if (cardInfo.Length == cardNumber.Length)
                {
                    bool isValidExpiredDate = IsValidExpiredDate(cardInfo, expiredDate);

                    if (isValidExpiredDate)
                    {
                        var cardNumerParam = new SqlParameter("CardNumber", SqlDbType.VarChar) { Value = cardNumber };
                        var expiredParam = new SqlParameter("ExpiredDate", SqlDbType.Char) { Value = expiredDate };
                        var procResult = await _unitOfWork.CardRepository.ExecWithStoreProcedureAsync("usp_GetCreditCard @CardNumber, @ExpiredDate"
                            , new[] { cardNumerParam, expiredParam });

                        if (procResult != null && procResult.Where(x => x.CardNumber == cardNumber && x.ExpiredDate == expiredDate).Any())
                        {
                            validationResult = new CardValidationResponse
                            {
                                ErrorMessage = string.Empty,
                                CardType = cardType.ToString(),
                                Result = ValidateStatus.Valid.ToString()
                            };
                        }
                        else
                        {
                            validationResult = new CardValidationResponse
                            {
                                ErrorMessage = "Card not found",
                                CardType = cardType.ToString(),
                                Result = ValidateStatus.DoesNotExist.ToString()
                            };
                        }
                    }
                    else
                    {
                        /* Return Invalid card */
                        validationResult = new CardValidationResponse
                        {
                            ErrorMessage = "Invalid expired date",
                            CardType = cardType.ToString(),
                            Result = ValidateStatus.InValid.ToString()
                        };
                    }
                }
                else
                {
                    /* Return Invalid card */
                    validationResult = new CardValidationResponse
                    {
                        ErrorMessage = "Invalid",
                        CardType = cardType.ToString(),
                        Result = ValidateStatus.InValid.ToString()
                    };
                }
            }
            else
            {
                /* Return Unknown card */
                validationResult = new CardValidationResponse
                {
                    ErrorMessage = "Unknown",
                    CardType = cardType.ToString(),
                    Result = ""
                };
            }

            return validationResult;
        }


        /// <summary>
        /// Convert validation rule data to generic card information rule.
        /// </summary>
        /// <returns></returns>
        private List<CardTypeInformation> GetCreditCardRuleFormat(List<CreditCardTypeRule> creditCardRuleList)
        {
            List<CardTypeInformation> ruleList = new List<CardTypeInformation>();
            foreach (var info in creditCardRuleList)
            {
                string rexEx = string.Empty;
                if (info.IsRuningNumber.GetValueOrDefault())
                    rexEx = GetRexFormat(info.DigitStart.GetValueOrDefault(), info.DigitEnd.GetValueOrDefault());
                else
                    rexEx = info.RegEx;

                CardType cardType = (CardType)Enum.CardType.Parse(typeof(CardType), info.CardType, true);
                ruleList.Add(new CardTypeInformation(rexEx, info.Length.GetValueOrDefault(), cardType, info.ExpireLeapYear.GetValueOrDefault(), info.ExpirePrimNumber.GetValueOrDefault()));
            }

            return ruleList;
        }


        /// <summary>
        /// Return regular expression format.
        /// </summary>
        /// <param name="digitStart"></param>
        /// <param name="digitEnd"></param>
        /// <returns></returns>
        private string GetRexFormat(int digitStart, int digitEnd)
        {
            StringBuilder regBuilder = new StringBuilder();
            regBuilder.Append("^(");
            for (int i = digitStart; i <= digitEnd; i++)
            {
                regBuilder.Append(i);
                if (i < digitEnd)
                    regBuilder.Append("|");
            }
            regBuilder.Append(")");
            return regBuilder.ToString();
        }

        /// <summary>
        /// Check if credit card number is valid card type
        /// </summary>
        /// <param name="cardInfomationList"></param>
        /// <param name="cardNumber"></param>
        /// <returns></returns>
        private CardType GetCardType(List<CardTypeInformation> cardInfomationList, string cardNumber)
        {
            foreach (var info in cardInfomationList)
            {
                if (Regex.IsMatch(cardNumber, info.RegEx))
                    return info.Type;
            }
            return CardType.Unknown;
        }


        /// <summary>
        /// Valiate card expired year include prime number year and leap year.
        /// </summary>
        /// <param name="cardInfor"></param>
        /// <param name="expiredDate"></param>
        /// <returns></returns>
        private bool IsValidExpiredDate(CreditCardTypeRule cardInfor, string expiredDate)
        {
            bool isLeapYear = Convert.ToBoolean(cardInfor.ExpireLeapYear);
            bool isPrimYear = Convert.ToBoolean(cardInfor.ExpirePrimNumber);

            string year = expiredDate.Substring(2, 4);
            if (!isLeapYear && !isPrimYear)
                return true;
            else if (isLeapYear)
                return IsLeapYear(year);
            else if (isPrimYear)
                return IsPrimeNumber(year);

            return false;
        }

        /// <summary>
        /// Return true or false if year is leap.
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        private bool IsLeapYear(string year)
        {
            int expiredYear = Convert.ToInt32(year);
            if ((expiredYear % 4 == 0 && expiredYear % 100 != 0) || (expiredYear % 400 == 0))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Return true or false if year is prime number.
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        private bool IsPrimeNumber(string year)
        {
            int expiredYear = Convert.ToInt32(year);
            if (expiredYear % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(expiredYear));

            for (int i = 3; i <= boundary; i += 2)
                if (expiredYear % i == 0)
                    return false;

            return true;
        }

    }
}
