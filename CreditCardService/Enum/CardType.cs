﻿namespace CreditCardService.Enum
{
    public enum CardType
    {
        Unknown = 0,
        Master = 1,
        Visa = 2,
        Amex = 3,
        JCB = 4,
    }

    public enum ValidateStatus
    {
        Valid,
        InValid,
        DoesNotExist
    }
}
