﻿using CreditCardService.Enum;
namespace CreditCardService.Data
{
    public class CardTypeInformation
    {
        public string RegEx { get; set; }
        public int Length { get; set; }
        public CardType Type { get; set; }
        public bool ExpiredLeapYear { get; set; }
        public bool ExpiredPrimNumber { get; set; }

        public CardTypeInformation(string regEx, int length, CardType type, bool expiredLeapYear, bool expiredPrimNumber)
        {
            RegEx = regEx;
            Length = length;
            Type = type;
            ExpiredLeapYear = expiredLeapYear;
            ExpiredPrimNumber = expiredPrimNumber;
        }
    }
}
